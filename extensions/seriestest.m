function [ isrand, count, low, high ] = seriestest( A )
    n=length(A);
    count=0; % ���������� �����
    n1=0; % ���������� �������
    n2=0; % ���������� ������
    B=sign((A)-median(A));
    for j=1:1:500
        if(B(j)<0)
            n1=n1+1;
        end
        if(B(j)>0) 
            n2=n2+1;
        end
    end

    tmp = sqrt(2*n1*n2*(2*n1*n2-n)/(n*n*(n-1)));
    low=2*n1*n2/n+1-1.96*tmp;
    high=2*n1*n2/n+1+1.96*tmp;

    for i=2:1:500
        if(B(i-1)*B(i)<0)
            count=count+1;
        end
    end
    
    if(count>low && count<high)
        isrand = true;    
    else
        isrand = false;    
    end
end

